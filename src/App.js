import React from 'react';
import './App.css';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';
import Home from "./components/home";
import Contacts from "./components/contacts";
import About from "./components/about";

import ListWrapper from './components/ListWrapper';

class App extends React.Component {



  render(){

    return (
      <div className="App">
        <BrowserRouter>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/list">List</Link>
              </li>
              <li>
                <Link to="/contacts">Contacts</Link>
              </li>
              <li>
                <Link to="/about">About</Link>
              </li>
            </ul>
          </nav>

          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/list" component={ListWrapper} />

            <Route exact path="/contacts" component={Contacts} />
            <Route exact path="/about" component={About} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }

}

export default App;
