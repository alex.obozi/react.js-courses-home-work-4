import React from 'react';
import { Switch, Route } from 'react-router-dom';

import List from "./list";
import ListItem from "./list-item";


const ListWrapper = () => (
    <Switch>
      <Route exact path="/list" component={List} />
      <Route exact path="/list/:itemId" component={ListItem} />
    </Switch>
);

export default ListWrapper;
