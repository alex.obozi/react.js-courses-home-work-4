import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class List extends Component{

  state = {
    usersData: []
  }

  componentDidMount() {

    fetch('https://jsonplaceholder.typicode.com/users')
      .then(res => res.json())
      .then(res =>
        this.setState({
          usersData: res
        })
      );
  }

  render(){
    const { usersData } = this.state;
    return (
      <div>
        List

        <ul>
          {
            usersData.map( item => (
              <li key={item.id}>
                <Link to={`/list/${item.id}`}>
                  {item.name}
                </Link>
              </li>))
          }

        </ul>
      </div>
    );
  }

}

export default List;
