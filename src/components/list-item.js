import React, {Component} from 'react';

class ListItem extends Component {

  state = {
    userData: []
  }

  componentDidMount() {

    fetch(`https://jsonplaceholder.typicode.com/users/${this.props.match.params.itemId}`)
      .then(res => res.json())
      .then( res => (
        this.setState({
          userData: res
        })
      ));
  }

  render() {

    const {userData} = this.state;

    return (
      <div>
        <p>Name: { userData.name }</p>
        <p>Username: { userData.username }</p>
        <p>E-mail: { userData.email }</p>
        <p>Phone: { userData.phone }</p>
        <p>Website: { userData.website }</p>
      </div>
    );
  }
}

export default ListItem;
